#!/bin/bash

#variables


if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  return
fi


# show "other" user in user folder
sudo defaults write /Library/Preferences/com.apple.loginwindow SHOWOTHERUSERS_MANAGED -bool TRUE

# enable hidden users
sudo defaults write /Library/Preferences/com.apple.loginwindow Hide500Users -bool YES

# disable guest account
sudo defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool NO


/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install openssl &
brew install wget &
brew install cask
brew cask install google-chrome
brew cask install slack &
brew cask install dropbox &
brew cask install sublime-text &
open -a "Google Chrome" --args --make-default-browser
killall "Googoe Chrome"

#install OL Desktop & Meraki Agent
directory=`pwd`
mkdir "temp_mio_software"
cd "temp_mio_software"
curl "https://bitbucket.org/Andrewmaledy/software/get/f98e2149f3fb.zip" -o "software.zip"
unzip "software.zip"
cd "Andrewmaledy-software-f98e2149f3fb"
sudo installer -pkg MerakiSM-Agent-systems-manager.pkg -target /
sudo installer -pkg Microsoft_Office_2016_15.29.16120900_Installer.pkg -target /

sudo hdiutil attach OneLoginDesktopInstaller.dmg
cd $directory
#rm -rf "temp_mio_software"
/Volumes/OneLoginDesktopInstaller_2_0_23/OneLogin.app/Contents/MacOS/OneLogin &
# Add printers ?