<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onelogin {

	public $request;
	public $response;
	public $data;
	public $error;
	public $request_url;
	public $http_method;
	private  $access_token;
    
    public function __construct()
    {
    	
    	require_once 'vendor/httpful.phar';
        $this->ci =& get_instance();
     	$this->ci->config->load('onelogin');



    }

    /**
    *
	* Get an access token from OneLogin and save it
	*
	*
    */    
    private function refresh_access_token(){

		
		$uri = $this->ci->config->item('onelogin_api_auth_url');
		$data = '{"grant_type" : "client_credentials"}';
		$response = \Httpful\Request::post($uri)                  // Build a PUT request...
	    ->sendsJson()
	    ->addHeader("Authorization:client_id:{$this->ci->config->item('onelogin_client_id')}, client_secret:{$this->ci->config->item('onelogin_client_secret')}", null)
	    ->body($data)             // attach a body/payload...
	    ->send();	
		$this->access_token = $response->body->data[0]->access_token;

    }

    /**
    *
	* Binds the access token to the request. If an access token is not set, refresh it (request a new one and save it to our private class variable... see above function)
	*
	*
    */

    private function set_bearer_token(){

    	//ensure there is an access token set, if not refresh it.
    	if(is_null($this->access_token)){
    		$this->refresh_access_token();
    	}

        // print_r($this->access_token);
    	//add bearer token (access token) to the header
        // print_r($this->access_token);
        $this->request->addHeader("Authorization", "bearer:{$this->access_token}");
    }
    /**
	* 
	* Concatenates base_url to the uri segment of the specified api request (ie /users/12345, etc...)
	*
	*
    */
    private function concatenate_request_url($url){

    	$url =  $this->ci->config->item('onelogin_api_base_url').'/'.$url;
 
    	return $url;

    }


    public function request($url, $method='get', $data = null){
		
		
		
		$this->request = \Httpful\Request::$method($this->concatenate_request_url($url));
		$this->request->sendsJson();
		$this->set_bearer_token();
        
		if(!is_null($data)) $this->request->body($data);
		
        $response= $this->request->send();
        // print_r($this->request);
        return $response;
    	
    }

    // public function get_users($object = 'users'){

    // 	$uri = $this->concatenate_request_url('users');
    // 	$this->request = \Httpful\Request::get($this->concatenate_request_url($object));
    // 	$this->request->sendsJson();
    // 	$this->set_bearer_token();
    // 	return $this->request->send();
    
    // }

 	
}