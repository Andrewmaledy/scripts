<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slack {

	public $request;
	public $response;
	public $data;
	public $error;
	public $request_url;
	public $http_method;
	private  $access_token;
    
    public function __construct()
    {
    	
    	require_once 'vendor/httpful.phar';
        $this->ci =& get_instance();
     	$this->ci->config->load('slack');
    }

    /**
    *
	* Get an access token from Slack and output it (they never expire).
	*
	*
    */    
    public function refresh_access_token(){

		
		$uri = $this->ci->config->item('slack_api_auth_url');
		$client_id = $this->ci->config->item('slack_client_id');
		$client_secret = $this->ci->config->item('slack_client_secret');		
		$response = \Httpful\Request::get("https://slack.com/api/oauth.access?client_id=$client_id&client_secret=$client_secret&scope=admin&code=2533808511.105039968582.ea89be313d")                  // Build a PUT request...
	    ->send();
		// print_r($response->raw_body);
		// $this->access_token = $response->body->data[0]->access_token;

    }

    /**
    *
	* Get an access token from OneLogin and save it
	*
	*
    */    
    public function update_user($id, $profiledata){

    	$data = urlencode(json_encode($profiledata));

    	$token = $this->ci->config->item('slack_api_access_token');
		
		$uri = $this->ci->config->item('slack_api_base_url');
		$client_id = $this->ci->config->item('slack_client_id');
		$client_secret = $this->ci->config->item('slack_client_secret');		
		$response = \Httpful\Request::put("$uri/users.profile.set?token=$token&user=$id&profile=$data")// Build a PUT request...
	    ->send();
		// print_r($response->raw_body);
		// $this->access_token = $response->body->data[0]->access_token;

    }


    public function list_users(){

		$uri = $this->ci->config->item('slack_api_base_url');

    	$token = $this->ci->config->item('slack_api_access_token');
		// echo "$uri/users.list?token=$token";
		$response = \Httpful\Request::put("$uri/users.list?token=$token")// Build a PUT request...
	    ->send();
	    // print_r($response);
	    return $response->body;


    }


}