<?php 

class User_model extends Onelogin_Model{

	public $defaultFields;
	public $customFields;

	function __construct(){
		// Onelogin_model is autoloaded in /config/autoload.php so no need to call parent::__construct() (it gets called on the autoload);
		
		$this->modelName = 'users';
		$this->defaultFields = array('company', 'department','email','firstname','lastname', 'notes', 'phone', 'title','username');

		$this->customFields = array('displayName','alias','office', 'streetAddress','city','country', 'zipCoode','region','timezone', 'state', 'employeeStatus');
		
	}

	public function get($id = null){

		
		$result= is_null($id) ? $this->onelogin->request($this->modelName)->body : $this->onelogin->request("{$this->modelName}/$id")->body  ;
		// print_r($result);		
		if(isset($result->pagination)){
		
			$pager = $result->pagination;
		

			if($pager->after_cursor){
		
		//		echo "THERE ARE MORE ROWS";
				while ($pager->after_cursor) {				

					$r = $this->onelogin->request("{$this->modelName}?after_cursor={$pager->after_cursor}")->body;
					$pager = $r->pagination;
					$more_results[] = $r->data;	
				}

				foreach($more_results as $mr){
					$result->data = array_merge($result->data,$mr);

				}
				// $result->data = array_merge($result->data, $more_results);
			}
		
		}
	
		// print_r($result);
		// return $result;


		return $result;
	
	}

	public function get_user_by_email($email){
		//https://api.us.onelogin.com/api/1/users?email=hazel.zhang@onelogin.com
		
		return $this->onelogin->request("{$this->modelName}?email=$email");

	}

	public function set_group_membership($id, $group_id){

		$data = "{\"group_id\": $group_id}";	
		return $this->onelogin->request("{$this->modelName}/$id", 'Put', $data);
		
	}


	public function update($id, $data){

		return $this->onelogin->request("{$this->modelName}/$id", 'Put', $data);

	}

	public function update_custom_attributes($id, $data){

		return $this->onelogin->request("{$this->modelName}/$id/set_custom_attributes", 'Put', $data);
	}	

}

 ?>