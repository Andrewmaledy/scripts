<?php

class Onelogin_Model extends CI_Model{

	protected $modelName;

	function __construct(){

		parent::__construct();
		$this->load->library('onelogin');
	

	}



	public function get($id = null){

		
		$result= is_null($id) ? $this->onelogin->request($this->modelName)->body : $this->onelogin->request("{$this->modelName}/$id")->body  ;
		return $result;
	}


	public function getWhere($condition){

		return $this->onelogin->request("{$this->modelName}?$condition")->body;

	}



}

?>