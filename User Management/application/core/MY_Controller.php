<?php
class MY_Controller extends CI_Controller {
    
    protected $modelName;

    public function __construct() {
    
       parent::__construct();

    
    }

	protected function set_model($name){
		$this->modelName = $name;
		$this->load->model($name);
	}
 	/*
 	* 
 	*  Gets all records. If an ID is specified gets one type of record.
 	*
 	*/
 	public function get($id = null)
 	{

 		$path = isset($GLOBALS["argv"][4]) ? $GLOBALS["argv"][4] : null;		
		
		#if $id is not numeric we can assume user passed a file path.

		if(!is_numeric($id) && (!is_null($id))){

			$path = $GLOBALS["argv"][3]; #no [3] not [4] (like above), because user only passed 1 argument.
			$data = $this->{$this->modelName}->get();

		}else{
			$data = $this->{$this->modelName}->get($id);

		}
		if($data->status->code == 200){
			$data = $data->data;
		}
		return (!is_null($path) ? $this->export_as_json($path, $data) : print_r($data));

 	}

 	/*
 	* 
 	*  Get record matching condition string (as per OL documentation). If filepath is specified returns it as JSON, otherwise prints it to the command line.
 	*
 	*/
	function getWhere($condition){

		$path = isset($GLOBALS["argv"][4]) ? $GLOBALS["argv"][4] : null;

		
		$data = $this->{$this->modelName}->getWhere($condition);
		if($data->status->code == 200){
			$data = $data->data;
		};

		return (!is_null($path) ? $this->export_as_json($path, $data) : print_r($data));
	
	}

 



 	/*
 	* 
 	* Simply dumps array to a json file at a desired path.
 	*
 	*/
 	protected function export_as_json($path, $arr){

 		$fh = fopen($path, 'w');
 		fwrite($fh, json_encode($arr));
 		fclose($fh);

 	}
}
?>