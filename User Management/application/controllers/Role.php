<?php

class Role extends MY_Controller {

 	public function __construct(){

 		parent::__construct(); 	 		
 		// $this->modelName = 'roles_model';
 		parent::set_model('roles_model');
 	}

 	/*
 	* 
 	*  Gets all records. If an ID is specified gets one type of record.
 	*
 	*/
 	public function get($id = null)
 	{

 		$path = isset($GLOBALS["argv"][4]) ? $GLOBALS["argv"][4] : null;		
		
		#if $id is not numeric we can assume user passed a file path.

		if(!is_numeric($id) && (!is_null($id))){

			$path = $GLOBALS["argv"][3]; #no [3] not [4] (like above), because user only passed 1 argument.
			$data = $this->{$this->modelName}->get();

		}else{
			$data = $this->{$this->modelName}->get($id);

		}
		$data = $data->data;

		return (!is_null($path) ? $this->export_as_json($path, $data) : print_r($data));


 	}

}


