<?php
class SlackUsers extends MY_Controller {

 	public function __construct(){

 		parent::__construct();
 		$this->load->model('user_model');
 		//parent::set_model('user_model');

 	}

 	// goes through all users in slack, finds them in OneLogin, and sets their slack fields to whatever is set in OneLogin. It's a pain in the ass becuase slack's custo fields have weird keys...makes the code vey unreadable but I just had to do a "slack.com/api/users.list" and derive the key names from that.

 	public function sync_profiles(){
 		$this->load->library('Slack');
 		// $users = $this->user_model->get(29439201);
 		$slack_users = $this->slack->list_users();
 		// $slack_users = $slack_users;

 		foreach($slack_users->members as $user){
	 		// continue;
	 		// print_r($user);
	 		// die();
	 		if(!isset($user->profile->email)){
	 			continue;
	 		}
	 		$u = $this->user_model->getWhere("email={$user->profile->email}");	 		
	 		if(isset($u->data[0])){
	 		$u = $u->data[0];
	 			if(!isset($u->email)) continue;

	 			$slack_userdata = array(
	 					// 'first_name' => $u->firstname,
	 					// 'last_name'	 => $u->lastname,
	 					'fields'	=> array(
	 							'Xf30H67S2D' => array('value' => $u->title),
	 						
	 					
	 							'Xf30G8UNQY' => array('value' => $u->department),
	 						
	 
	 							'Xf30H6FF0R' => array('value' => $u->custom_attributes->streetAddress),
	 						
	 					
	 							'Xf30G9A7A4' => array('value' => $u->custom_attributes->city),
	 					
	 					
	 							'Xf317NAZD0' => array('value' => $u->custom_attributes->state),
	 					
	 					
	 							'Xf317NFYMQ' => array('value' => $u->custom_attributes->country),
	 					
	 					
	 							'Xf30H6T4TT' => array('value' => $u->custom_attributes->zipCode),

	 							'Xf37RJU7T8' => array('value' => (isset($u->custom_attributes->mobile) ? $u->custom_attributes->mobile : '' )),
	 							)
	 				);
 		

 		$this->slack->update_user($user->id, $slack_userdata);
		echo "Updated $u->firstname in Slack. \n";
		$slack_userdata = array();
	
 	
	}
 		
	}

 	}



}