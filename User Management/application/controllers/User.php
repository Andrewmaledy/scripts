<?php
class User extends MY_Controller {

 	public function __construct(){

 		parent::__construct();
 		parent::set_model('user_model');

 	}

 	/*
 	* 
 	*  Gets all records. If an ID is specified gets one type of record.
 	*
 	*/
 	public function get($id = null)
 	{

 		$path = isset($GLOBALS["argv"][4]) ? $GLOBALS["argv"][4] : null;		
		
		#if $id is not numeric we can assume user passed a file path.

		if(!is_numeric($id) && (!is_null($id))){

			$path = $GLOBALS["argv"][3]; #no [3] not [4] (like above), because user only passed 1 argument.
			$data = $this->{$this->modelName}->get();			
		}else{
			$data = $this->{$this->modelName}->get($id);

		}
	
		$data = $data->data;

		return (!is_null($path) ? $this->export_as_json($path, $data) : print_r($data));


 	}



	/*
 	* 
 	*  Updates a user record (takes file path argument for data)
 	*
 	*/

	function update(){

 		$path = isset($GLOBALS["argv"][3]) ? $GLOBALS["argv"][3] : null;		


		if(is_null($path)){
			echo "Please specify a json file with data to update.\n";
			exit(0);
		}

		
		$users = json_decode(file_get_contents($path));
		$dataDefaultFields = array();
		$dataCustomFields = array();

		for ($i=0; $i < count($users) ; $i++) { 
			
			foreach($this->{$this->modelName}->customFields as $field){
				if(isset($users[$i]->$field)) $dataCustomFields[$i]['custom_attributes'][$field] = $users[$i]->$field;
			}

			print_r($this->{$this->modelName}->update_custom_attributes($users[$i]->id, $dataCustomFields[$i])->raw_body);			
			echo "\n";
			foreach($this->{$this->modelName}->defaultFields as $field){
				if(isset($users[$i]->$field)) $dataDefaultFields[$i][$field] = $users[$i]->$field;

			}

			print_r($this->{$this->modelName}->update($users[$i]->id, $dataDefaultFields[$i])->raw_body);
			echo "\n";		


		}
		
	
	}
	//updates display names from first & last
	
	public function update_display_names(){

		$users = $this->onelogin->request("users");
		$users =$users->body->data;


		foreach($users as $u){
			$displayName = "{$u->firstname} {$u->lastname}";
			$data = "{\"custom_attributes\": {\"displayName\": \"$displayName\" }}";
			echo "$displayName ";
			echo $this->onelogin->request("users/{$u->id}/set_custom_attributes", 'PUT', $data)->raw_body;
			echo "\n";
		}

	}
  
 	public function addgroup($user_email = '', $group_id=''){


 		$data = file_get_contents('all_users.json');
 		$data = json_decode($data);
 		
 		// print_r($data);
 		foreach($data->data as $user){
 		

 			print_r($this->user_model->set_group_membership($user->id, 426017)->body);

 		}
 		// $user_id->body->data[0]->id;

 	}


 }